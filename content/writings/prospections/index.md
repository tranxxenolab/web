---
title: "Dearest Xen (Letters to Lichen)"
description: "Epistolary ruminations on xeur apothecia, among other things"
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "writings"
    - "lichen"
    - "xen"
date: 2021-12-07T12:20:50+01:00
publicationYear: "2021"
inProgress: false
draft: false
---

Commissioned by [BAK](https://www.bakonline.org/) for the Prospections issue on ["No Linear Fucking Time"](https://www.bakonline.org/focus/no-linear-fucking-time/), ["Dearest Xen (Letters to Lichen)"](https://www.bakonline.org/prospections/dearest-xen-letters-to-lichen/) explores a fascination, an obsession with lichen, xeur deeper times, xeur symbioses, and what this might mean for human-lichen intertwinings.

The piece can be read on the [Prospections journal website](https://www.bakonline.org/prospections/dearest-xen-letters-to-lichen/)
