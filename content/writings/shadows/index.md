---
title: "What We Do in the Shadows (Quaronatine Diaries)"
description: "Forced isolation, heteronormative nuclear families, and the possibilities of tranxxeno nestings"
slug: ""
image: ""
keywords: "xenology, heteronormativity, queer, quaronatine diaries"
categories: 
    - "Index"
    - "writings"
date: 2020-03-27T16:26:31-05:00
publicationYear: ""
inProgress: false 
draft: false
---

{{< figure src="images/forced_isolation.png" >}}

*this is an occasional series of short texts written from a xenological perspective during the quaronatine*

**27.03.2020**

We are in times where we face dramatic alterations to the dynamics of sociality. An agglomeration invisible to us confines our movements to the walls of our residences--but of course only for those who have a place to call "home". This agglomeration--the combined forces of SARS-CoV-2 and the semiotic diktats of the state--comes into being with no ability to dissent in a biopolitical sense. We face a requirement to acquiesce.

We must accept physical distancing and forced isolation.

The state commands us to stay-at-home--which makes epidemiological sense. When there are no defenses for this virus other than our unprepared immune systems, we have to do something to slow the virus' spread so as to not overwhelm our already-fragile healthcare systems or our immunocompromised kin.

Yet there is something extremely disturbing on a fundamental level about these demands to remain in "our home". Beyond the basic biopolitical facts of the state and its management of health statistics, of its attempted control over the parameters of life and death, the state here is additionally reifying, in a terrifying fashion, the heterosexual nuclear family. The home and all its connotations--the assumption of biological kinship and state-sanctioned co-habitation through marriage--becomes the place where we must isolate ourselves for the unforeseeable, unknown future. Schools disbanded and students returned to their families. A conservative wetdream of enforced familial bonds. No legislation or rightist movement alone could have produced this result--they needed the virus as a purported ally.

We can already understand some of the effects of this forced familial isolation. We read reports--if we take the time to look for them--of increased calls to domestic violence hotlines. We can easily imagine the horrors of a spouse intoxicated on liquor, angry and afraid at the loss of work and money, violently lashing out at their wife or child. We read on social media of the increased demands on mothers as they juggle housework, home schooling, a full-time job, and, in the worst of cases, an abusive spouse. We know that there are millions of queers living in a double isolation, one from the virus, the other from their vibrant lives as members of queer communities, now suppressed by dangerous family members. We know there are hundreds of thousands of trans people at home being deadnamed, being prevented from accessing their medications, their binders and gaffs, their social networks from school. We know that too many of these people are being violently attacked by their families.

So much for our forced isolation and the safety of the home.

This is not a simple argument that the cure is worse than the disease. But to not understand that this moment of forced isolation is in fact terrifying for reasons beyond the virus is to deny the lived experiences of trans, queer, and xeno peoples the world over. We are being denied the very parameters of the most vital aspects of our existences: the ability to experience relationships and kinships beyond the boundaries of heterosexuality and familial bonds. The reading groups in the cafe that help to set the basis for new forms of living; the comings and goings of people to and from the co-op that forge new relationships; the clothes and costumes and makeup that mark us as a fabulous other; the reverberating bodies intimately together underground in the club: these are the moments that are impossible now by order of the state. Zoom, Facebook, FaceTime, Skype: they provide merely shadows. Necessary shadows perhaps to get us through this time. But our technology remains so primitive as to not even allow proper touch at a distance.

We don't know how long this forced isolation will last. We don't know how much of the present moment may become permanent. We don't know if we may never again be physically near many of our far-flung loved ones across the globe. We don't know if xenophobia and isolationism may soon reign again, preventing gatherings of those who are fundamentally opposed to the primacy of national and familial identity.

As xenologists we know that there are other ways of being, other ways of structuring our social lives. We desire the mixing of boundaries, of making ourselves other, of bringing into existence new modes of relationship not only with our more-than-human kin--now imbued with another entity marked as SARS-CoV-2--but also the agglomerations of other living and non-living entities on this planet.

Right now we may be prevented from bringing these possibilities into being at scale. They may exist merely as talk between shadows. But we need to prepare for the moment in the future where there is an opening, as there will come a time when the orders to isolate will be lifted. How can we begin to build the structures so that we can self-isolate together when the next order to isolate comes again--for it will indeed come. How can we build places of respite for the tired xenoentities of the world, where we othered can be proximately supported by the othered within walls that the state would see as a "home". How can we build the technologies of communion across distance for the kin that we cannot touch in isolation on the other side of the world. How can we use our networks to construct, below the perception of the state, a series of linked nests that might form the basis for a new ordering of society.

These are not necessarily novel desires but this moment of forced isolation highlights the immediacy of the needs. We can no longer wait for some future to come when we will do this; that future is now.

SARS-CoV-2 wrecks destruction on more-than-human bodies--we must accept this as fact. Yet the virus can also be understood as our own kind of wily ally. As it brings out the worst in the state--military on the streets, closed borders, national hoarding, reification of the family, xenophobia--we xenologists can also choose to work with the vagaries of the virus to set the stage of our own future thriving. We may be in isolation and isolated from each other now. But if we-who-are-able take these months to plan, to study, to think, to connect, and to learn, we can leap into the sliver of opportunity to come. There we can collect together our hurt xenokin and hunker down again. There will likely be waves of isolation and movement in the decades to come, as the realities of climate change reach into the echelons of society. It behooves us now to gather us othered together--the tranxxeno. Separate, in dangerous familial structures, we may be forgotten and many will be destroyed. Together, in new dynamical agglomerations, we can live out our desired ways of being.

*Written by Adriana Knouf*
