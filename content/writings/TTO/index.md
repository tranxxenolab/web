---
title: "Temporal Transfer Orbit (TTO) Voiceover"
description: "Text of the voiceover presented in the film"
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "writings"
date: 2022-11-19T14:02:47+01:00
publicationYear: "2022"
inProgress: false
draft: false
---

"...colonization is a matter of forcible redirection of whole systems of social life, biology, politics, and material infrastructure" (11): Fred Scharmen's words echo in my head.

I wonder what our world would have been like in terms of economic systems, race relations, indigenous rights, sexual identity, and gender expression had colonization not ravaged the Earth. Goddess knows what might happen if we humans travel to another planet before figuring things out better here. 

Colonization made it so that so many of the colonized could not live as themselves.  Colonization of thought made it impossible for people like me to transition for decades. Colonization exists as not only control over bodies, but also control over the imagination. To decolonize is to not only repair the damages of the past but also to re-engage with the imaginations that have been suppressed. 

We need to get to a different orbit before we leave.

---

The Hohmann Transfer Orbit is the lowest-energy way to travel between two large bodies in the solar system. An arc of an ellipse between two circular orbits. It just requires two small pushes–delta-v–at the beginning and the end of the trip.

Delta-v...it's the fundamental term in space navigation, the amount your velocity has to change to go from one orbit to another–"delta" standing for "change in". Delta-v dominates us and there’s no safe word to set us free.

Once you're in your final circular orbit, though, unless there is drag or some other force acting upon you, once you’re in that orbit you will stay there, forever. Circling around, and around, and around, and around. Unless you get another push through another Delta-v. The domination is relentless.

I think about my life and what Delta-v's have been needed to push me out of static, boring, repetitive, harmful orbits. Of the stable orbit that I was in for decades that accepted forced masculinity and presumed that girlhood was forever off limits. But it wasn't just a Delta-V that got me out of that rutted orbit, the locked groove that had set the boundaries of my imagination. I needed more, a new kind of transfer orbit that incorporates other parameters. Call it a Temporal Transfer Orbit, for lack of a better term. 
This orbit not only includes the Delta-v, but also:

* Delta-t, a change in time
* Delta-h, a change in history
* Delta-i, a change in imagination

Such an orbit may take the same physical trajectory in space as a Hohmann transfer, but it will still do it differently. And most importantly, it will create things now that need to exist in order for a desired future to come to pass. Maybe in this Temporal Transfer Orbit we can begin to repair the mistakes of the past and live a bit more attuned to the possibilities of joy and commensal living with the universe.

---

I’m looking at the Mission Requirements document for  a recent space telescope; it has subheadings like "Telemetry", "Lifetime", and "Operational Orbit". These are obviously important engineering requirements to fulfill.

But I also pull out the Mission Requirements document for my mission "TX-2: MOONSHADOW". Here I've written a different set of subheadings: 

* Mission Requirement: SOLAR SAIL, 
* Mission Requirement: MARS LICHEN, 
* Mission Requirement: QUEER. 


I wonder what would happen to our space activities if we extended what we considered to be a "requirement" to those domains of our lives that are just as necessary as "Telemetry".


---

I currently have an ambiguous relationship to chaos magick and manifestation of reality through the construction of sigils. Perhaps that’s a sign itself that I shouldn't be working with AIs to create sigils in the first place–at least not until I've figured out my own positioning. But I also feel that the development of these sigils in the MOONSHADOW project has charged these designs with a power that they didn't have before. Certainly the ones on our flatpack antenna design speak loudly to me about both protection--which is what they were charged with at the moment of inception--as well as joy, which has been the other thread throughout sigil creation. I'm now considering getting a tattoo of one or more of these sigils–which I guess in the end highlights the power they have taken on over these past few months.

---

The irony of being obsessed with solar sails in a project about decolonization is not lost upon me. If anything, massive sailing vessels were one of the most damaging technologies of the age of colonization. But, as of yet, solar sail spacecraft have not participated in such terrible activities. To me there's something incredibly romantic--yes, romantic--about unfurling a humongous solar sail a few tens of a micron thick that exists to reflect the photons from the sun. 

The whole mechanism is confounding when you think about it. Quantum mechanics tells us photons don’t have mass, yet they have momentum...which can be transferred to the sail when the photons bounce off of it. And thus, each photon imparts a tiny bit of force to the sail, pushing it forward. Bounce enough of these photons off the sail for a long enough time--for years, decades--and you can gain quite a bit of speed. 

Maybe this is a way to be a bit more humble about space travel, to recognize that we don’t always have to find the quickest way from point A to B–especially if there aren't humans on board.

---

Travel to an extraterrestrial world, such as Mars, in the human form we have now, is quite a silly thing to do. Lack of oxygen, low or no air pressure, super-fine toxic dust, continuous cosmic radiation exposure--the list of things that can and will kill you is endless. The engineers say that future travelers can overcome these challenges with external technology–spacesuits, habitats, terraforming--but I'm skeptical. They seem to have forgotten what Manfred Clynes and Nathan Kline argued for back in the 1960s, which was the transformation, through mechanical and biological means, of humans for the space environment--rather than the other way around. They argued for the creation of the cyborg.

A character in Becky Chambers' novella *To Be Taught, If Fortunate*, undergoes such a process, termed "somaforming", through the wearing of transdermal patches. The character describes it like this: "I believe somaforming is the most ethical option when it comes to setting foot off Earth. I'm an observer, not a conqueror. I have no interest in changing other worlds to suit me. I choose the lighter touch: changing myself to suit them." (12) It's perhaps interesting to consider the similarities between the transdermal somaforming patches and the transdermal estrogen patches that older trans women like myself have to wear for the rest of our lives.

---

But because our transdermal estrogen patches don't quite reach the level of the somarforming patches, and because biohacking still has a ways to go--maybe humans shouldn't travel to Mars at all right now. Maybe certain planets or moons aren't for us. But maybe they’re actually just right for other organisms. Like my current obsession, lichen. A form of directed panspermia, or the spreading of life from one planetary body to another. 

Lichen for sure would do better on Mars than humans. I know this idea seriously offends people, so much so that some have said my project is unethical. But if we’re so worried about contaminating other planets or moons, should we send humans at all? Our fragile, infected, dirty bodies? To take life to another world--whether it’s lichen or human--is to contaminate that world in some way. It's better we accept that now and decide what kinds of contamination we’re okay with rather than burying the question under the guise of human exceptionalism.

---

Because I have an ambiguous relationship to human consciousness I find myself out in the universe a lot more than I find myself rooted to the earth. Maybe that’s also why I'm obsessed with creating an entire space mission--requirements, spacecraft, hardware, software, materials--to the detriment of romantic relationships, the search for human companions, and even my own mental health. But perhaps this whole process is also about trying to manifest a world where doing what I do is done alongside other similarly obsessed humans. I have a sense sometimes that I'm making work for a world that I won't live in. That somehow, through a heightened sense of hubris, what I do might actually help manifest a Temporal Transfer Orbit that causes our current locked orbit to swerve elsewhere. In the end, I think it's going to need a lot more pushes than just my own. But I'm going to continue making and manifesting, in the earnest hope that our orbits might dramatically change during the timeline of my own life.

# References

Chambers, Becky. *To Be Taught, If Fortunate*. Hodder & Stoughton, 2019

Scharmen, Fred. *Space Forces: A Critical History of Life in Outer Space*. Verso Books, 2021
