---
title: "Fragments of Xenology"
description: "A constellation of xenological encounters"
slug: ""
image: ""
keywords: ""
categories: 
    - "writings"
    - "pamphlets"
date: 2021-04-28T14:12:31+02:00
publicationYear: "2019-2021"
inProgress: false
draft: false
---

*First in the occasional tranxxeno lab pamphlet series*

*Fragments of Xenology* conducts an expansive series of encounters with the term "xenology": the study, analysis, and development of the strange, the alien, the other. The text engages with questions of how and why to undergo xenomogrification for the purposes of disalienation; the relationships between transgender people, hormones, and the alien; xenological erotics; non-semiological interlocutions; and other forms of xenological concept formation.


[**The complete text is available as a downloadable PDF.**](media/Knouf_FragmentsOfXenology.pdf)

<!--
# inhuman|human>

Xenology is different from posthumanism is different from the anthropocene is different from the chthulucene (Haraway 2016). Xenology eschews the linearity indexed by a "post-", even if such an indexing is assumed only in the prefix. The idea that our time is now "after" something called "humanism" implies that "humanism" was a useful exercise in the first place. It wasn't. We have never been human--at least for some of us. Xenology draws upon the vital living power of those deemed inhuman in order to foreground the inhumanity of all "humans". We are all entwined with non-human others: our guts provide the living conditions for trillions of bacteria, our existence depends on the relative cosmic "quietness" of our solar system, our geopolitical systems are dependent on the distribution of rare amounts of metals crushed by the earth's crust, themselves dependent on exploding stars from billions of years ago.

The notion of the "anthropocene" and all of the other "-cene"s are also immense misnomers. While it is undoubtedly true that what-we-call-humans have altered the geological strata of the earth, this has only occurred *in concert* with other elements of the earth: radioactivity, condensed forms of energy from beings that died millions of years ago, rare minerals. The counterfactual position--that a similar sort of momentous change in geological strata would not have occurred had we not begun the Industrial Revolution--is impossible to prove, given that we presently have access to only one potential historical timeline. Thus to lay the blame for this on "humans" is to participate only in a specious form of tallying the accounts, an activity that will be forgotten in a thousand years. Rather, we have to begin to *humble* ourselves, knowing that to the universe we are no more special than an electron that winks into and out of existence from the vacuum field. The question then becomes: do we want to re-modify the earth--and ourselves with it--so that it is again habitable to us and other forms of life? This humbling also needs to realize that life on earth is likely to persist if we were removed from the equation--life is imminently adaptable.

Our "humanity" is predicated on that-which-is-not-human. We are all thus "other" to the concept of the "human"^[Chen, 2012, 42--50. See also Braidotti, 2013.]. Of course processes of othering other *Homo sapiens* have caused unbelievable amounts of pain, harm, destruction, and death to billions of people. We will be grieving these loses for thousands of years. But the long-term solution is not to hold on to the concept of the "human" that marks distinctions between ourselves and the rest of the cosmos. As a horizon, we need to push towards a discarding of the "human" concept, both for our own sake, and for the sake of the earth and other entities in the solar system and beyond. The splitting of "human" from "nature" requires us to weave back together what capitalism has divided, in the words of Silvia Federici^[Silvia Federici, "Re-enchanting the World: Feminism and the Politics of the Commons". Presentation at Tufts University, 12 February 2019.], Perhaps we need a new term, one that recognizes the superposition of our existence, the existence of other lives, the existence of not-alive-matter, the existence of the universe^[I am using the word "superposition" here in the scientific sense, where two or more things are made to coincide or coexist in the same place, such as when waves combine with each other: "The action of causing two or more sets of physical phenomena (e.g. waves, colours, or motions) to coincide, or coexist in the same place; the fact or an instance of such coexistence". See the OED definition for "superposition".]. 

Yet we also need to simultaneously fight for an acceptance of all *as human* in the present moment, so as to amplify the voices silenced for too long. The challenge for xenology as a politico-aesthetic practice is to hold both components together simultaneously. We cannot misunderstand the praxis of short-duration tactics to make *human* life better in the here-and-now. Doing so does not prevent concurrent xenological developments. In order to transform we have to begin with something; 0|xenologist> is still 0. If we fail at this most basic thing, we will have failed the aliens nearest us and allowed the logics of sameness to prevail. To play the long game xenologists must still exist in some form.
-->
