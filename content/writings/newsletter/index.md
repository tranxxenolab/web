---
title: "tranxxeno lab interlocution newsletter"
description: "occasional writings sent to you via e-mail"
slug: "newsletter"
image: ""
keywords: ""
categories: 
    - "Index"
    - "writings"
date: 2024-11-10T11:31:08+01:00
publicationYear: ""
inProgress: true
draft: false
---

There are two newsletters you can subscribe to:

* _letters from the tranxxeno lab_, which is your standard informational newsletter that highlights important news from the lab (upcoming and past exhibitions, talks, workshops, and performances), as well as other updates. The frequency of this is around 6-12 times per year.
* _fragments of xenology_ will offer more insights into the work of the laboratory, such as an intriguing experiment, fascinating artwork or machine, fragment of writing-in-progress, sound snippet, frustrating failure mode. This frequency of this newsletter is indeterminate, but will likely be more often than the aforementioned _letters_. Nevertheless, each transmission will also likely be shorter.

You can see previous newsletters [here](https://letters.tranxxenolab.net/).

Please enter your e-mail below to signup for either or both of the newsletters. You can easily unsubscribe at anytime. **We will never share your e-mail with anyone.** All data is stored in the EU and processed according to the GDPR.

{{< rawhtml >}}
<div style="height: 40vh"><script src="https://cdn.jsdelivr.net/ghost/signup-form@~0.2/umd/signup-form.min.js" data-label-1="tranxxenolab website" data-background-color="#ffffff" data-text-color="#000000" data-button-color="#eb008b" data-button-text-color="#FFFFFF" data-title="tranxxeno lab interlocution" data-description="Xenological encounters" data-icon="https://letters.tranxxenolab.net/content/images/size/w192h192/size/w256h256/2024/11/apple-touch-icon_rotated.png" data-site="https://letters.tranxxenolab.net/" async></script></div>
{{< /rawhtml >}}
