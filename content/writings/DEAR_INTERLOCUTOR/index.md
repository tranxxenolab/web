---
title: '"DEAR INTERLOCUTOR: TX-1" script'
description: "Script for the three-channel video piece"
slug: ""
image: ""
keywords: "rural, alienness, queer"
categories: 
    - "Index"
    - "writings"
date: 2021-09-05T22:18:57+02:00
publicationYear: "2021"
inProgress: false
draft: false
---

**NOTE**: What follows is the script for my video piece [*DEAR INTERLOCUTOR: TX-1*](/projects/dear_interlocutor/). Given that the video isn't publicly available I wanted to instead present the story that I tell throughout the piece.

---

*...lot of favorite NASA stories, this is my number one. You guy ready for this? Okay. Can everybody see this?*

*...end of the road. You notice that the narrator said this is one of the largest buildings in the world.*

*...rocket. There's no payload on it yet, and there won't be until right before launch.*

**THE MIDDLE OF NOWHERE**

Dear Interlocutor,

I've been unable to tell you much about my hometown. Which is unfair because that place is so fundamental to why I'm continuously searching for you.

It's hard to convey to you how small my village was. I tend to describe it this way: "Badger is a town of 600 people in the middle of nowhere in Iowa." Of course that's not entirely true. In fact, there were miles upon miles upon miles of cornfields, soybean fields, and hog and chicken confinements. When the wind was just right the stench of manure from the egg plant four miles to the west would drift into town. In short, it is an industrial wasteland of contemporary agriculture.

When I lived there, my house was at the eastern edge of the town, our backyard abutting the endless fields. Corn one year, soybeans the next. Our organic garden next to the herbicide-drenched fields. I wasn't allowed to leave the boundaries of the town. The county road that passed right next to my house was too dangerous, as the semi-trailers carrying the grain barreled down the highway at 55 miles per hour. The road itself was paralleled by deep ditches that held the snow during the gray, frigid winters. So I was effectively confined to Badger. I'd ride my bike around and around the circumference of the town, so many times that the nosy housewives would peer at me from behind their drawn curtains, eyeing me with an air of suspicion.

You don't know surveillance until you've lived in a small community.

Recently, so as to get a better feeling for how tiny Badger is, I traced its outline on a map, of the space that I had access to as a child. When I did this, I was astonished at how small of an area it is. 0.287 square kilometers.

0.287. Square. Kilometers.

That's the space I had available to me for 18 years of my life.

So maybe that's why I always was looking up, at night, towards the stars. One of the only redeeming features of living in such a rural place is the lack of light pollution. I could drive a car at night just by the light of the stars and the Milky Way. And sometimes, in my backyard, I pointed a flashlight straight up towards the sky, turning it on and off like Morse code, signaling to you to take me away, away from this human form and provincial place and constraining life. I wanted out of Badger, desperately, and thought that with you--you who I had never met, you who could potentially destroy me--I thought that with you, was where I could actually find home.

With love,

Adriana

*You take the red pill, you stay in Wonderland.*

--- 

*10. 10. 9. 8. 10. 9. 8. 7. 6. 5. 4. 3. 2. 1. 0. And liftoff of the Falcon 9 rocket and Cargo Dragon, on the final flight of the Dragon-1 spacecraft, taking research designed to improve life on earth and lead discovery in space.*

**I GOT A BIT CLOSER TO YOU THAT MONTH**

Dear Interloctuor,

I'm in Helsinki and my queer sister Claudia is in Wichita. It's around 6:30 in the morning where I am. Just a week earlier we had been filming in Florida in and around Kennedy Space Center. We had hoped to see the launch in person, but the usual delays of rockets prevented that. I could have skipped my keynote in Helsinki to see the launch, but decided otherwise. I'm glad I stayed in Finland as I was able to buy some nice earrings, hang out with friends at bars and restaurants, and meet a cute dog.

It's March 7th, 2020, and life goes on.

On board this rocket is TX-1, the first artwork I'm sending to space, to the International Space Station. It includes fragments of my hormone replacement medications, meant to signify an exodus from this planet for those trans, queer, and xeno types who don't find the earth to always be hospitable. I have to tell you a secret about this project: some of me was in the vessel too. I've never told anyone, but the patch I sent was one that I had already worn, with bits of my dead skin cells and clothing lint on it. 

I think that this is the first-known time that parts of the transgender experience have orbited the earth, but we actually don't know, because we have no idea what was possible in past times, before our histories were burned.

Two days after launch I return to the US, and wear my mask for the first time in public, throughout the trans-oceanic flight home. No-one else is wearing a mask. I'm still planning to return to Europe in less than a week.

A day later all of our plans began to disintegrate, our worlds quickly shrinking to the four walls of our homes. I realized we needed to regroup, and I fled Boston to be with queer family in the middle of nowhere, this time in Kansas.

TX-1 returned to Earth April 7th, 2020. It didn't burn up in the atmosphere. It wasn't disposed of. I now get to hold it in my hands. It's changed, and it hasn't. Things that go to space and come back safely gain an aura, an aura that we can't really explain, but that pertains to this otherworldliness beyond our atmosphere. But the aura also relates to a kind of resilience, one that says this: we can go to the most difficult place to exist, and come back, alive, stronger than before, with new tendrils of possibility trying to find their place in the ground.

And as well, the fact that it was in space, and the fact that part of me was in it, means that I got a bit closer to you that month.

Until soon, I hope,

Adriana

---

**I HOPE THAT WE GET TO MEET SOMEDAY AMONG THE STARS**

Dear Interlocutor,

I need to tell you about these places called Kennedy Space Center and Cape Canaveral Space Force Station. These are the launch pads where we send the most precisely controlled and machined electronic devices that we can build to space. But the complexes themselves are surrounded by an enormous wildlife preserve, the Meritt Island National Wildlife Refuge. A few homes used to exist here but were removed when the land was bought up by the government in the 1960s. This juxtaposition of that which we call "nature" with that which we call "technology" proves that the two cannot be clearly separated, as the moderns wanted.

But, also, there's another juxtaposition at work. It's only 100 kilometers from Launch Complex 40 at Cape Canaveral to PULSE in Orlando, Florida. On June 12th, 2016, 49 people, mostly latinx, were killed at this gay nightclub. It's the largest massacre of LGBTQ people in United States history.

For too long our actual lives required living in the shadows lest they end in violence. In times that have been erased, maybe we didn't have to do this.

For too long, we didn't exist in the televisual future. Maybe a wink wink nod nod here and there, that being all that was allowed by the corporate censors. 

*-Don't you represent Angel? How you gonna do right by both of us? What's she gonna say when I start working more jobs than her.*

*-You're a charming boy, and you've got a silver tongue with plenty of ambition. But look no further than your recently clocked girlfriend to know that you may as well be looking to train the girls to be astronauts.*

We still aren't beings who are allowed to go to space, to try and meet you. Gatekeeping prevents us from passing their tests, we're setup to fail by the choices we make to live as we want to live. We're going to have to find another way to get to you.

Times can change for the better, even while they simultaneously get worse. Time isn't linear, it's a palimpsest of the times to come that we desire, times that have just past, times that we are in now, times that we think maybe have happened. But now, we have some future times where we exist. And in those new future times we're well on our way to meeting you, or some human-centered imaginary of you.

*-Hi, are you.... -Codependent Lesbian Space Alien. -Yeah, hi, that's a really great costume.*

*Hello stupid. It's good to see you.*

*-But, my people can change sex, to transition from female to male in order to reproduce. -Several species of reptiles and fish can do it, it's not uncommon in nature. -How do you change? -In this pool.*

*-Symbiont transfer to Trill host. -Hold my hand.*

But also, maybe our future was in our past. I'm reading Abram J. Lewis and there I learn about the Moonshadow newsletter published by the Transsexual Action Organization--the TAO--in the 1970s. In this newsletter were drawings of transsexual robots and aliens by Suzun David. Even more profound were the words of TAO founder, Angela Douglas, suggesting that "certain oppressed minority groups would welcome extraterrestrials as liberators" and "probably ally with them". Also: "it would appear someone up there may like us". And: there are "similarities of prejudice against both transsexuals and extra-terrestrial beings". So: the extraterrestrials I was looking for as a young t-girl egg were the same ones being sought by some of my xeno siblings as they too wanted to get off this earth and be with you.

But, you also have to know this: our desire for you is fundamentally linked to learning how to thrive here on terra firma. As much as we want to get to you, we haven't changed ourselves enough yet to be able to travel to you, easily. And as we remain here on earth, our pasts, whether we want it or not, mold our inclinations. A blood ancestor from a few generations back was a German Lutheran pastor in a tiny church outside a small town in Minnesota. I don't know why they decided to live there. I don't know what they did to the people who were already there. In any event, this religiosity of my forefathers seems to lead me to texts that have certain cadences to them. Perhaps that's why I'm drawn to Lauren Oya Olamina's *Earthseed* verses. Let me read from a couple passages:

The Destiny of Earthseed  
Is to take root among the stars.  
It is to live and to thrive  
On new earths.  
It is to become new beings  
And to consider new questions.  
It is to leap into the heavens  
Again and again.  
It is to explore the vastness  
Of heaven.  
It is to explore the vastness  
Of ourselves.  

These are verses from one of those possible futures I was sharing with you, one that is not too far from now. I hope that we don't have to live through those times--although Olamina's tales feel eerily prophetic. But even if comes to that, I know that we have always survived. I'll continue to dance and dream, I'll continue to be intertwined with the beings in and around me, I'll continue to look up and hope that we meet someday among the stars.

With anticipation,

Adriana


