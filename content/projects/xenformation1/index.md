---
title: "Xenformation 1: What Does Xen Dream of in the Theatricum?"
description: ""
slug: ""
image: "art4med_xenformation.png"
keywords: "performance, Waag"
tagline: "Performance about trans bodies in ancient art, the history of the Waag, and transformation"
categories: 
    - "Index"
    - "projects"
date: 2022-09-16T19:30:00+02:00
draft: false
---

{{< load-photoswipe >}}

# Xenformation 1: What Does Xen Dream of in the Theatricum?

{{< gallery >}}
    {{< figure caption="AI generated image for performance and exhibition" src="images/xenformation1_001.jpg" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_002.png" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_003.png" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_004.png" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_005.png" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_006.png" >}}
    {{< figure caption="Still from video projected during the performance" src="images/xenformation1_007.png" >}}
{{< /gallery >}}

Time isn't linear. Touches from the past vibrate out of inert statues, charging our present with the recollection that what appears new today actually has ancient roots. Bolts from futures that we desire force us to figure out what catalysts need to be gathered now to make those possibilities occur. The heavy air of a complicated space requires massaging to make it amenable to new flows. Transformation becomes xenformation as actions drawing from art history, queer history, speculative futures, and biological research merge together in a performance of protocols for living within transitioning times.

Encompassing sound, light, projections, poetry, and bioart, the performance manifests reparative protocols for the Theatricum Anatomicum of Waag and invites people to understand their own potentials for xenformation.

The performance took place once, on 16 September 2022 at 19:30 in Waag's Theatricum Anatomicum.

The text presented during the performance, as well as the video projected, are available on request.

# Credits

Concept, text, video, sound, performance: Adriana Knouf

Set design: Antonia Steffens

Technical assistance, lighting: Nienke Tjallingii

Moderator: Mini Maxwell

Special thanks: Claudia Pederson, Lucas Evers, Sunke Puell, Beatriz Sandini, Špela Petrič

# Funding

Funded in part by a 3Package Deal award from the Amsterdams Fonds Voor de Kunst
and Art4Med.
