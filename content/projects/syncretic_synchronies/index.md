---
title: "Syncretic Synchronies"
description: ""
slug: ""
image: "syncretic000.jpg"
keywords: ""
tagline: "mixings of the ancient and the modern, terrestrial and extraterrestrial"
categories: 
    - "Index"
    - "projects"
date: 2024-09-16T16:22:27+02:00
draft: false
---

{{< load-photoswipe >}}

# Syncretic Synchronies (working title)

{{< gallery >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic000.jpg" >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic001.jpg" >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic002.jpg" >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic003.jpg" >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic004.jpg" >}}
    {{< figure caption="Syncretic Synchronies (process documentation)" src="images/syncretic006.jpg" >}}
{{< /gallery >}}

*Syncretic Synchronies* (working title) is a project-in-process, funded by a residency from the [European Media Art Platform](https://emare.eu/) in Yogayakarta, Indonesia. Working closely with Venzha Christ of the [Indonesia Space Science Society](https://vufoc.space/) I was able to collaborate with a number of artists, designers, musicians, writers, philosophers, and thinkers explore the intertwining of ancient understandings of Javanese philosophy and spirituality with contemporary explorations of the extraterrestrial and the alien. Coupled with my own interests in this area, along with other broader currents around the alien and transgender life in trans archives, I am developing an installation that includes the works of my collaborators along with my own, including a video work that contains footage from the residency, explorations of Javanese cosmology from the perspective of my collaborators, ruminations on aliens, and the potentials for changing the framework of so-called "western" practices that acknowledges our own spiritual and occult pasts and how the willful forgetting of those pasts limits the development of the scientific, technical, and philosophical worlds we live in.

*Syncretic Synchronies* is still in development and is looking for exhibition possibilities. Please contact me if you are interested.

Collaborators: Hangno Hartono, Nanang Garuda, Ramberto Gozalie, Dian Graha, Yonz Dickinsons, Setyawan Haryanto, Bon Diaz, Venzha Christ.
