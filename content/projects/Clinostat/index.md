---
title: "Open Source Clinostat"
description: ""
slug: "clinostat"
image: "clinostat.jpg"
keywords: ""
tagline: "Designs, software, and hardware for microgravity simulation"
categories: 
    - "eromatase"
    - "projects"
    - "biofriction"
date: 2021-06-08T08:58:22+02:00
draft: false
---

{{< load-photoswipe >}}

# Open Source Clinostat

{{< gallery >}}
    {{< figure caption="Open source clinostat in the BioTehna laboratoy @ Kersnikova Institute, during Adriana Knouf's Biofriction residency; credit Hana Jošić" src="images/clinostat_000.jpg" >}}
    {{< figure caption="Adriana in the BioTehna laboratory @ Kersnikova Institute, during her Biofriction residency; credit Hana Jošić" src="images/clinostat_001.jpg" >}}
    {{< figure caption="Adriana in the BioTehna laboratory @ Kersnikova Institute, during her Biofriction residency; credit Hana Jošić" src="images/clinostat_002.jpg" >}}
    {{< figure caption="Adriana in the BioTehna laboratory @ Kersnikova Institute, during her Biofriction residency; credit Hana Jošić" src="images/clinostat_003.jpg" >}}
    {{< figure caption="Open source clinostat in the BioTehna laboratoy @ Kersnikova Institute, during Adriana Knouf's Biofriction residency; credit Hana Jošić" src="images/clinostat_004.jpg" >}}
    {{< figure caption="Open source clinostat in the BioTehna laboratoy @ Kersnikova Institute, during Adriana Knouf's Biofriction residency; credit Hana Jošić" src="images/clinostat_005.jpg" >}}
{{< /gallery >}}

*this project is part of* [Xenological Entanglements. 001: Eromatase](/proejcts/eromatase)

Sending biological projects to space is expensive and challenging, even with enormous resources. For artists, it's nigh impossible without specialised relationships with governmental or commercial entities, meaning that a lot of interesting possibilities for experimentation are limited to those with lots of capital (monetary and social). 

But it's also possible to simulate certain aspects of the microgravity--or "weightless"--environment here on earth. One of the most common ways for simulating microgravity for research involving mammalian cells is the ["rotating wall vessel" (RWV)](https://lsda.jsc.nasa.gov/Hardware/hardw/1248) (also sometimes called the "fast rotating clinostat" or "clinostat" for short), originally developed by NASA in the 1980s. These devices have also been used in recent decades for growing 3D tissue cultures (and have been seen within the bioart context, most notably with the Tissue Culture and Art Project's [*Semi-living Worry Dolls*](https://tcaproject.net/portfolio/worry-dolls/) (2001)). The theory of the devices is relatively simple in concept, but complicated in its details (Briegleb 1992). The idea is that cells grow in medium embedded in a liquid of high viscosity, all within a device that is rotating at a constant speed. At a certain number of revolutions per minute, the cells in the growth medium will not be traveling around the circumference of the vessel, but will rather appear "stationary", even as the cells inside are constantly in a state of rotation. Taking a time average of their gravitational vector, this will average out to zero--thus simulated weightlessness. At no point are the cells actually in a state of free fall, but rather it is the *time average* that is zero. But various experiments have shown that this is a relatively good and robust means of simulating microgravity (Cogoli 1992; Klaus 2001; Hemmersbach, Wiesche, and Seibt 2006; Grimm et al. 2014).

Nevertheless, the costs of commercial clinostats can be prohibitive. A reusable, autoclavable 10mL RWV from a supplier such as [Synthecon](https://synthecon.com/pages/autoclavable_vessel_culture_systems_synthecon_23.asp) can run upwards of €1700--and this is just for the vessel alone, and not for the control box and associated hardware. Again, this is cost-prohibitive for artists or biohackers on limited budgets.

Because of these reasons, we decided to create an open version of the clinostat or RWV within the ethos of transhackfeminism--namely, making these research tools free and accessible for others to further the queering of science and bioart, especially within the outer space context. This work was one of the main outcomes of my [Biofriction](https://biofriction.org/) residency, ["Xenological Entanglements. 001: Eromatase"](https://tranxxenolab.net/projects/eromatase/), produced by [Kersnikova Institute/Kapelica Gallery](https://kersnikova.org/) in the fall of the pandemic year 2020.

The elements of this open design are:

* Reverse-engineered vessel for growing cells, made out of glass and teflon (in our version);
* Custom 3D-printed frame for mounting the vessel and rotating it;
* Electronics hardware to control the vessel's rotation and provide some visual indicators of the machine's properties, installed in two external control boxes; and,
* Software for controlling the rotational speed of the vessel, providing an interface for various sensors, and allowing for future expansion (including logging of data/sensor readings, remote monitoring, etc.).

All of these elements are integrated together and designed in such a way that the frame and vessel can be placed inside most incubators, with the control boxes remaining outside.

# Documentation

The documentation for the open-source clinostat can be downloaded [here](documents/Knouf_OpenSourceClinostat_Pamphlet.pdf). This documentation outlines the rationale, the materials, the assembly and fabrication process, the pitfalls, and the challenges of building an open-source clinostat. The documentation was produced by Kersnikova as part of the Biofriction project; many thanks for their assistance!

# Repositories

I have split out the separate components to individual repositories on Gitlab:

* [Design files for the vessel, frame, and box](https://gitlab.com/tranxxenolab/clinostat-design)
* [Schematics and layout for clinostat control board](https://gitlab.com/tranxxenolab/clinostat-control-board)
* [Software for clinostat control board](https://gitlab.com/tranxxenolab/clinostat)

<!-- All of the repositories will be complete soon! Please [e-mail me](mailto:asknouf@tranxxenolab.net) if you're interested in the project and/or need access *right now*. -->

# Credits

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance, Kristijan Tkalec for his expert scientific assistance, and Meta Petrič for her hands-on assistance. Special thanks to Špela Petrič.

Created during a [Biofriction](https://biofriction.org/) residency.

Produced by [Galerija Kapelica](https://kersnikova.org/about-us/kapela/)/[Zavod Kersnikova](https://kersnikova.org/) in Ljubljana, Slovenia.

Biofriction is supported by the European Commission--Creative Europe.

# References

Briegleb, Wolfgang. "Some Qualitative and Quantitative Aspects of the Fast-Rotating Clinostat as a Research Tool." ASGSB Bulletin 5, no. 2 (1992): 23–30.

Cogoli, Marianne. "The Fast Rotating Clinostat: A History of Its Use in Gravitational Biology and a Comparison of Ground-Based and Flight Experiment Results." American Society for Gravitational and Space Biology Bulletin 5, no. 2 (1992): 59–67.

Grimm, Daniela, Markus Wehland, Jessica Pietsch, Ganna Aleshcheva, Petra Wise, Jack van Loon, Claudia Ulbrich, Nils E. Magnusson, Manfred Infanger, and Johann Bauer. "Growing Tissues in Real and Simulated Microgravity: New Methods for Tissue Engineering." Tissue Engineering Part B: Reviews 20, no. 6 (2014): 555–66. <https://doi.org/10.1089/ten.teb.2013.0704>.

Hemmersbach, Ruth, Melanie von der Wiesche, and Dieter Seibt. "Ground-Based Experimental Platforms in Gravitational Biology and Human Physiology." Signal Transduction 6, no. 6 (December 2006): 381–87. <https://doi.org/10.1002/sita.200600105>.

Klaus, David M. "Clinostats and Bioreactors." Gravitational and Space Biology Bulletin 14, no. 2 (2001): 55–64.

