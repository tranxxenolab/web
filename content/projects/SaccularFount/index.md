---
title: "Xenological Entanglements. 001b: Saccular Fount"
description: ""
slug: "saccular_fount"
image: "saccular_fount_000.jpg"
keywords: ""
tagline: "Photoperformance and speculative externalized hormonal production"
categories: 
    - "eromatase"
    - "projects"
    - "biofriction"
date: 2021-06-08T10:55:00+02:00
draft: false
---

{{< load-photoswipe >}}

# *Xenological Entanglements. 001b: Saccular Fount* (2020)

{{< gallery >}}
    {{< figure caption="Saccular Fount documentation @ Kapelica Gallery, during a Biofriction residency; credit Andrej Lamut" src="images/saccular_fount_000.jpg" >}}
    {{< figure caption="Saccular Fount photoperformance @ Kapelica Gallery, during a Biofriction residency; credit Andrej Lamut" src="images/saccular_fount_001.jpg" >}}
    {{< figure caption="Saccular Fount photoperformance @ Kapelica Gallery, during a Biofriction residency; credit Andrej Lamut" src="images/saccular_fount_002.jpg" >}}
    {{< figure caption="Saccular Fount documentation @ Kapelica Gallery, during a Biofriction residency; credit Andrej Lamut" src="images/saccular_fount_003.jpg" >}}
    {{< figure caption="Saccular Fount documentation @ Kapelica Gallery, during a Biofriction residency; credit Andrej Lamut" src="images/saccular_fount_004.jpg" >}}
    {{< figure caption="Saccular Fount permanent installation @ Kersnikova Institute; credit Andrej Lamut" src="images/saccular_fount_005.jpg" >}}
    {{< figure caption="Saccular Fount permanent installation @ Kersnikova Institute; credit Andrej Lamut" src="images/saccular_fount_006.jpg" >}}
    {{< figure caption="Saccular Fount permanent installation @ Kersnikova Institute; credit Andrej Lamut" src="images/saccular_fount_007.jpg" >}}
{{< /gallery >}}

*this project is part of* [Xenological Entanglements. 001: Eromatase](/projects/eromatase)

She needs what it provides, badly. She’s intoxicated by the estrogen molecule, a rather simple thing really, yet a compound that has deep biophysical effects in the right concentrations. A molecule that is now being produced just outside her body, in a sacculus, in microgravity, in a simulation of the outer space environment that she wishes to experience.

This fount of estrogen means she is free of the pharmaceutical companies. This fount bypasses the gatekeepers who titrate the molecule to those who fit within the categories they have defined.

Yet she remains burdened by the saccular fount’s weight, its unwieldiness, its entanglements with the non-human entities that are unceremoniously enrolled in her craving for a thriving life. She wishes for something more commensal.

She knows that others may also desire what this saccular fount provides, and thus she opens up its designs for expansion and mutation.

Presented as a speculative wearable device, Saccular Fount suggests a near-term future where personal hormone production is both possible and necessary.

---

Within growth medium saturated with testosterone, testicular Leydig cells convert this hormone to estradiol using the enzyme aromatase. Thus, cells that come from a part of the body associated with masculinity  the testicles  are induced to produce estradiol  which is associated with femininity. These cells additionally grow in two custom-made rotating wall vessels that simulate a microgravity condition, thereby linking the wearer of Saccular Fount to the potential of weightlessness in outer space and the possibility of sourcing hormones for long-duration space missions.

Saccular Fount is worn in the center of the torso, between the breasts, and hangs on a custom-made leather harness. This harness both constricts the body and suspends the device, highlighting a particular tension between the molecular jouissance induced by estradiol in transgender bodies as well as the constraints in how these (and other) bodies are seen in society.

Given challenges in access to hormones for people on various kinds of hormone replacement therapy (including transgender or perimenopausal people), Saccular Fount additionally gestures towards another means of hormone production separate from pharmaceutical companies. Yet this very production relies on cells sourced from non-human mammals, and thus the additional materials used in the piece  leather, pig intestines  palpably foregrounds these concerns.

Saccular Fount is a waypoint within the broader Eromatase project, taking stock of developments so far, pondering future commensal relationships with organic materials, and re-imagining biological destiny.

# Documentary Video

{{< youtube 7qzEKCO6MAo >}}

*Saccular Fount* is discussed within this documentary video about my Biofriction residency.

Video: Hana Jošić

# Credits

Expert scientific consulting and assistance: Kristijan Tkalec, dr. Neža Grgurevič

Laboratory assistance: Lotti Gosar

Device design: Lovrenc Košenina

Device and harness machining and construction: David Pilipovič, Jože Zajec

Hands-on assistance: Meta Petrič

Photo: Andrej Lamut

Hair & Makeup: Lea Bratušek

Photo-performans ambient, set-up, and technical assistance: Mirjam Čančer, Anže Sekelj, Jure Sajovic

Special thanks: Lea Aymard, Špela Petrič, Zavod TransAkcija

---

Created during a [Biofriction](https://biofriction.org/) residency.

Produced by [Galerija Kapelica](https://kersnikova.org/about-us/kapela/)/[Zavod Kersnikova](https://kersnikova.org/) in Ljubljana, Slovenia.

Supported by: the European Union – Creative Europe, the Ministry of Culture of the Republic of Slovenia and the Municipality of Ljubljana – Department for Culture.
