---
title: "TX-1"
description: ""
slug: "tx-1"
image: "TX-1_001.png"
keywords: ""
tagline: "Space Sculpture"
categories: 
    - "TX-1"
    - "projects"
date: 2019-11-12T23:49:58-05:00
draft: false
---

{{< load-photoswipe >}}

# TX-1

{{< gallery >}}
    {{< figure caption="TX-1, after return from space; spheres had been moved to a different vessel for weight reasons; credit, Adriana Knouf" src="images/TX-1_return000.png" >}}
    {{< figure caption="TX-1, after return from space; spheres had been moved to a different vessel for weight reasons; credit, Adriana Knouf" src="images/TX-1_return001.png" >}}
    {{< figure caption="TX-1, after return from space; spheres had been moved to a different vessel for weight reasons; credit, Adriana Knouf" src="images/TX-1_return002.png" >}}
    {{< figure caption="TX-1, three resin spheres with embedded materials, each approximately 4-5mm in diameter; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_001.png" >}}
    {{< figure caption="TX-1, assembled pocket; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_002.png" >}}
    {{< figure caption="TX-1, spironolactone fragment embedded in resin sphere, approximately 4-5mm in diameter; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_003.png" >}}
    {{< figure caption="TX-1, Vivelle Dot patch fargment embedded in resin sphere, approximately 4-5mm in diameter; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_004.png" >}}
    {{< figure caption="TX-1, handmade abaca paper sculpture embedded in resin sphere, approximately 4-5mm in diameter; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_005.png" >}}
    {{< figure caption="TX-1, diassembled pocket" src="images/TX-1_006.png" >}}
    {{< figure caption="TX-1, for scale; pre-launch photo; credit, Adriana Knouf" src="images/TX-1_007.png" >}}
{{< /gallery >}}

**TX-1 was given the [Award of Distinction in Prix Ars Electronica 2021](https://archive.aec.at/prix/showmode/67778/)!!!**

**TX-1 launched to the ISS on 2020-03-07 at 0450UTC**!

**TX-1 returned to earth on 2020-04-07 at 1850UTC!**

The enchanting Earth is too-often made inhospitable to those marked as transgender. To survive we xenomogrify ourselves through social and biological technologies, altering our surfaces, our viscera, our molecular balances. None of us have been to space even if we possess somatic knowledges of deep bodily transformations, necessary experiences for extraterrestrial environments.

_TX-1_ launches bits of my hormone replacement medications, marking the first-known time that elements of the transgender experience orbit the earth. _TX-1_ includes a fragment of my spironolactone pill, a slice of my estradiol patch, and a miniature handmade paper sculpture, included to gesture towards the absent-yet-present xenoentities of the cosmos. A symbolic exodus to an orbit high above, the eventual return of _TX-1_ to Earth is also a sign of resilience, of not being disposed, of coming back to thrive once again.

This project is selected through MIT Media Lab Space Exploration Initiative’s first international artwork open call to the ISS and the launch opportunity is provided by the initiative. I also want to acknowledge support from faculty research funds at Northeastern University.

Please contact <asknouf@tranxxenolab.net> for press inquiries.

# Awards

![](images/PRX_Prix_AE2021_Distinction_500px.png)

# Video

{{< gallery >}}
    {{< vimeo 524018480 >}}
{{< /gallery >}}


# Documentation from the ISS

## Photos

{{< gallery >}}
    {{< figure caption="Sojourner2020, image from sculpture onboard the ISS; credit, Xin Liu, MIT Media Lab Space Exploration Initiative" src="images/ISS_001.jpg" >}}
    {{< figure caption="Sojourner2020, image from sculpture onboard the ISS; credit, Xin Liu, MIT Media Lab Space Exploration Initiative" src="images/ISS_002.jpg" >}}
{{< /gallery >}}

## Video

{{< gallery >}}
    {{< vimeo 405125237 >}}
{{< /gallery >}}

## Infos

We received video and images from the Sojourner2020 sculpture from the ISS during its time onboard the station.

Credit: Xin Liu, MIT Media Lab Space Exploration Initiative.

# Credits

Concept and fabrication: Adriana Knouf

TX-1 box design and modeling: Felipe Rebolledo

Additional modeling and rendering: Adriana Knouf

Kennedy Space Center footage: Adriana Knouf

Sojourner2020 images and ISS footage: Xin Liu, MIT Space Exploration Initiative

Launch and orbit footage: NASA

Editing: Adriana Knouf

TX-1 was selected through MIT Media Lab Space Exploration Initiative’s first international artwork open call to the ISS and the launch opportunity was provided by the initiative.

Additional support: Northeastern University

Special thanks: Claudia Pederson, Špela Petrič, Miha Turšič

# Other credits

![MIT Media Lab Space Exploration Initiative](images/SE_logo_Black_RGB_resized.jpg)

![MIT Media Lab](images/MIT_ML_Symbol_K_RGB_resized.jpg)

