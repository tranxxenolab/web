---
title: "TX-2: MOONSHADOW"
description: ""
slug: ""
image: "TX-2_cover.png"
keywords: "TX-2, cubesate, satellite, lichen, Mars"
tagline: "Solar sail cubesat for queer and trans decolonial futurities"
categories: 
    - "Index"
    - "projects"
date: 2022-09-22T19:30:00+02:00
draft: false
---

{{< load-photoswipe >}}

# TX-2: MOONSHADOW

{{< gallery >}}
    {{< figure caption="TX-2:MOONSHADOW full installation view as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_001.png" >}}
    {{< figure caption="TX-2:MOONSHADOW spacecraft as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_002.png" >}}
    {{< figure caption="TX-2:MOONSHADOW back view of spacecraft as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_003.png" >}}
    {{< figure caption="TX-2:MOONSHADOW spacecraft detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_004.png" >}}
    {{< figure caption="TX-2:MOONSHADOW cubesat detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_005.png" >}}
    {{< figure caption="TX-2:MOONSHADOW cubesat detail (rear) as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_006.png" >}}
    {{< figure caption="TX-2:MOONSHADOW sail detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_007.png" >}}
    {{< figure caption="TX-2:MOONSHADOW sail detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_008.png" >}}
    {{< figure caption="TX-2:MOONSHADOW QUEER vitrine as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_009.png" >}}
    {{< figure caption="TX-2:MOONSHADOW solar sail supercut video and ground station as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_010.png" >}}
    {{< figure caption="TX-2:MOONSHADOW SOLAR SAIL vitrine as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_011.png" >}}
    {{< figure caption="TX-2:MOONSHADOW MARS LICHEN vitrine as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_012.png" >}}
    {{< figure caption="TX-2:MOONSHADOW sail detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_013.png" >}}
    {{< figure caption="TX-2:MOONSHADOW sail detail as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_014.png" >}}
    {{< figure caption="TX-2:MOONSHADOW full installation view as part of : REWILD at MAXXI; photo credit: Adriana Knouf" src="images/TX-2_015.png" >}}
    {{< figure caption="TX-2:MOONSHADOW, machined aluminum and SLS 3D printed 6U cubesat structure shown during development; design by Felipe Rebolledo; photo credit: Adriana Knouf (cell phone camera)" src="images/TX-2_cubesat_001.png" >}}
    {{< figure caption="TX-2:MOONSHADOW, machined aluminum and SLS 3D printed 6U cubesat structure shown during development; design by Felipe Rebolledo; photo credit: Adriana Knouf (cell phone camera)" src="images/TX-2_cubesat_002.png" >}}
    {{< figure caption="Early render of project; image credit, Felipe Rebolledo" src="images/TX-2_render.png" >}}
{{< /gallery >}}

**ON VIEW @ [MAXXI](https://www.maxxi.art/en/events/repairing-the-present-rewild/) FROM 14 OCTOBER 2022 UNTIL 13 NOVEMBER 2022**

*TX-2: MOONSHADOW* is an experiment in speculative satellite construction. By its existence, it shows that space missions can be constructed for queer, post-colonial futures, contrary to contemporary commercial, military, and expansionist ends. Consisting of a handmade solar sail, custom 6U cubesat covered in protective sigils for transgender and queer joy, and a sculpted "meteorite" that contains lichen-human hybrid structures, *TX-2: MOONSHADOW* draws upon the history of transgender desires for extraterrestrial encounters and suggests possibilities for non-human experiences of outer space.

TX-2: MOONSHADOW was developed in the context of Decolonising Mars, a residency hosted by Art Hub Copenhaguen addressing the imaginaries and implications of space travel and migration, focusing on the environmental, social and political inheritance carried by those leaving Earth. Expert Fabrication Assistance: Giorgia Piffaretti. 3D Modeling and Industrial Design: Felipe Rebolledo. Spacecraft Fabrication Space: Georg Rasmussen, CLICK/Kulturværftet, Helsingør, Denmark. Accommodations: Georg Rasmussen, CLICK/Kulturværftet. SEM Scans of Lichen: Bo Thorning, FabLab RUC. Supported by Planetarium Copenhagen.

The Fellowship is co-commissioned by Art Hub Copenhagen_within the framework of Repairing The Present. Repairing The Present is co-funded by the STARTS program of the European Union.

# Vitrine documents

These documents explain some of the materials in the vitrines at MAXXI:

* Left vitrine, [Mission Requirement QUEER](pdfs/queer.pdf)
* Middle vitrine, [Mission Requirement SOLAR SAIL](pdfs/solar_sail.pdf)
* Right vitrine, [Mission Requirement MARS LICHEN](pdfs/lichen_mars.pdf)

# Video

{{< vimeo 835444746 >}}
