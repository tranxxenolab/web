---
title: "Temporal Transfer Orbit (TTO)"
description: ""
slug: ""
image: "TTO.png"
keywords: "TTO, TX-2"
tagline: "A meditation on space colonialism, queer transformation, and non-human space travel, told through archival footage and intimate shots of project TX-2:MOONSHADOW"
categories: 
    - "Index"
    - "projects"
date: 2022-11-03T22:22:02+01:00
draft: false
---

{{< load-photoswipe >}}

# Temporal Transfer Orbit (TTO)

{{< gallery >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO001.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO002.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO003.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO004.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO005.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO006.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO007.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO008.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO009.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO010.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO011.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO012.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO013.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO014.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO015.jpg" >}}
    {{< figure caption="TTO video still; photo credit: Adriana Knouf" src="images/TTO016.jpg" >}}
{{< /gallery >}}

**ON VIEW @ [ZKM](https://zkm.de/en/exhibition/2022/11/repairing-the-present-retool) AS PART OF : RETOOL FROM 18 NOVEMBER 2022 UNTIL 18 DECEMBER 2022**

Duration: 21'54''

Presented as 270 degree panoramic or 3-channel video, 2-channel audio

Low energy orbits around the problems of this planet cause too many of us to repeat the mistakes of the past, as if we were in a locked groove or a never-ending nightmare. Colonialism, racism, exploitation, homophobia, transphobia, fascism: no matter the desire to avoid these negative attractors, these orbits allow such behaviors to continue to fester within us, again and again as we circle around and around. What if we could shift to a new orbit through a change in trajectory? What energy would be needed to do this? Like the Hohmann Transfer Orbit (HTO) that allows navigators to find efficient trajectories for spacecraft to go to other planets, a Temporal Transfer Orbit (TTO) places us on a different vector, one that breaks free from the repetition of the same and navigates us to another spacetime. Such a Temporal Transfer Orbit requires inputting energy—researching, practicing, living. Learning from the [*TX-2: MOONSHADOW*](/projects/tx-2/) project by the artist, this exemplar of a Temporal Transfer Orbit lays in a trajectory for manifesting new approaches to life in outer space—which is the same as saying, life on Earth.

*Temporal Transfer Orbit (TTO)* was developed in the context of Decolonising Mars, a residency hosted and co-commissioned by Art Hub Copenhaguen addressing the imaginaries and implications of space travel and migration, focusing on the environmental, social and political inheritance carried by those leaving Earth. The residency benefited from a partnership with the Planetarium (Copenhagen, Denmark) and Click Festival (Helsingør, Denmark). Editor and camera: Giorgia Piffaretti. Editing, camera, digital imagery, sound: Adriana Knouf. Expert Fabrication Assistance: Giorgia Piffaretti. 3D Modeling and Industrial Design: Felipe Rebolledo. Spacecraft Fabrication Space and accommodations: Georg Rasmussen, CLICK/Kulturværftet, Helsingør, DK. SEM Scans of Lichen: Bo Thorning, FabLab RUC. Special thanks: Claudia Pederson, Rose Tytgat, Lars Bang Larsen, Jacob Fabricus, Mads Kring, Fahad Saeed. 

The Fellowship is co-commissioned by Art Hub Copenhagen_within the framework of Repairing The Present. Repairing The Present is co-funded by the STARTS program of the European Union.

# Screener

A screener is available on request.

# Video

{{< vimeo 835444746 >}}
