---
title: "Xenological Entanglements. 001: Eromatase"
description: ""
slug: "eromatase"
image: "patches.png"
keywords: ""
tagline: "Simulated microgravity + estradiol expression"
categories: 
    - "eromatase"
    - "projects"
    - "biofriction"
date: 2020-04-09T11:22:17-05:00
draft: false
---

# Xenological Entanglements. 001: Eromatase

{{< gallery >}}
    {{< figure caption="Adriana Knouf, wearing her Vivelle Dot estradiol patches (2020); credit, Adriana Knouf" src="images/patches.png" >}}
{{< /gallery >}}

**Supported by a [Biofriction residency](https://biofriction.org/news/open-call-results/) and produced by [Kersnikova Institute](https://kersnikova.org/)/[Kapelica Gallery](http://kapelica.org/index_en.html) in Ljubljana, Slovenia**

Works under this project include [*Xenological Entanglements. 001a: Trying Plastic Variations*](/projects/trying_plastic_variations), [*Xenological Entanglements. 001b: Saccular Fount*](/projects/saccular_fount), and the [Open Source Clinostat](/projects/clinostat).

# Documentary Video

{{< youtube 7qzEKCO6MAo >}}

This documentary details the current state of the *Eromatase* project and covers the outcomes of my Biofriction residency.

Video: Hana Jošić

---

**Original project description:**

To date, only 11.5% of astronauts have been women; only a couple are publicly known to have been queer; and none are known to be transgender. Little research has been done on the specifics of sex and gender in relation to spaceflight. The cost to send experiments to space has decreased due to new commercial launch providers, but still remains prohibitively expensive for most entities. Governmental agencies and commercial space ventures have made it clear that incorporating LGBTQ people and perspectives is not a priority. No research has been done on whether it is safe for transgender people on hormone replacement therapy (HRT) to go to space. For deep space missions there will need to be a source of exogenous estradiol, for both transgender women on HRT as well as as a component of hormonal birth control.

For these reasons it's necessary for us to develop our own space research programs independent of larger organizations. "Xenological Entanglements: 001. Eromatase" is the first ambitious project of this program. It consists of two parts. First is the development of an open-source microgravity simulator (random positioning machine and/or clinostat), where the hardware, software, and documentation will be shared publicly so that others can easily build their own. Second, the artist aims to genetically engineer her own testicular Leydig cells to enable the over-activation of aromatase. This will induce the Leydig cells to overproduce estradiol, thus enabling an assigned-male-at-birth body to self-produce the levels of estrogen required to live in a "female" body. These cells will be cultured under simulated microgravity using the equipment developed in the first part of the project.

"Eromatase" is part of the tranxxeno lab, a nomadic artistic research laboratory that promotes entanglements between entities trans and xeno. An entrancing beacon that demarcates a different trajectory for space development, "Eromatase" enacts research that would otherwise be deemed ancillary. Infused with the knowledge that deep space materials comprise the atomic source of life on earth, the project presents the tranxxeno dream of outer space as a place of exodus and thriving for all those who find life on this earth inhospitable.

### Credits

The residency was developed as part of the Biofriction project. 

Production: [Kapelica Gallery](http://kapelica.org/index_en.html)/[Kersnikova Institute](https://kersnikova.org/); co-production: [City of Women](http://cityofwomen.org/). Special thanks: Department of Art + Design, Northeastern University. Supported by: the European Union Creative Europe, the Ministry of Culture of the Republic of Slovenia and the Municipality of Ljubljana Department for Culture.
