---
title: "Quanto Canto"
description: ""
slug: ""
image: "thumbnail_1000px.png"
keywords: ""
tagline: "Sonic manifestation of quantum noise using just intonation tuning"
categories: 
    - "Index"
    - "projects"
date: 2025-02-23T12:43:20+01:00
draft: false
---

{{< load-photoswipe >}}

# Quanto Canto

{{< youtube 6rq_QVFc2PY >}}

{{< gallery >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto001.png" >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto002.png" >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto003.png" >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto004.png" >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto005.png" >}}
    {{< figure caption="Quanto Canto, installation view as part of 'Quantum Visions' at Tabakalera, Donostia/San Sebastián, Spain" src="images/quanto_canto006.png" >}}
{{< /gallery >}}

*sapeli wood, copper, brass, speakers, quantum computer output, custom electronics*

Approximately 3,5m x 1,5m x 1,5m

2-channel 8-speaker audio

2025-

**On view as part of [Quantum Visions](https://www.tabakalera.eus/en/visiones-cuanticas/) at Tabakalera, Donostia/San Sebastián, Spain, until 8 June 2025**

---

1. Uncertainty is a fundamental property of the cosmos.
1. Noise in measurement arises because of uncertainty.
1. Harmonic oscillators in quantum systems possess anharmonicity because of noise and design.
1. Harmonic oscillators in sonic systems possess anharmonicity because of noise and design.
1. Anharmonic oscillators thus allow for infinite gradations because of noise and design.
1. Manipulation of matter guides the selection of specific gradations of the anharmonicity.
1. A material basis for all things, and a homeomorphism between modes of description, implies little distinction to be made between oscillators in quantum systems and oscillators in sonic systems.
1. Is the frequency distribution of the universe one that we desire?

---

Considering "the concept of the entire universe viewed as a quantum computer, it becomes possible, if not feasible, to consider the exact frequency distribution of the present state of the universe (and, extrapolating, guessing the initial frequency ratios within some tolerance of error)." (Catherine Christer Hennix, Hilbert Space Shruti Box (of the (Quantum) Harmonic Oscillator), 2007)

Many quantum computers function on the principle of harmonic oscillators that are "detuned" a bit...they are rather *an*harmonic oscillators. Quantum computers are also intimately affected by the noise of the cosmos. Their computation is not "perfect"--no matter the desires of their makers. *Quanto Canto* manifests these tensions through sonic and sculptural means. The sounds come from a set of hardware sonic harmonic oscillators (a custom-designed modular synthesizer module) which highlight the odd harmonics. Measurement of 127 highly entangled qubits--and their deviation from the "ideal"--provides the noise that modulates various harmonics of the sonic oscillators.  The tuning changes throughout the piece, drawing inspiration from just intonation works such as LaMonte Young's *Well-Tuned Piano*.  As well, the second section of the piece is tuned using an anharmonic "octave" that relates to qubit transition frequencies. This is tuned in 61-limit just intonation. The sculptural form references existing quantum computer "chandelier" designs, transmon qubits, and organ pipes. The sculpture is made out of sapeli wood, copper, and brass.  Magickal sigils that highlight the superposition of identities found within queer and trans lives adorn the central pipes. 

*Quanto Canto* is the first prototype of a proposed "Quantal Chamber" which, drawing from immersive sound works like the *Dream House*, would allow people to feel the quamtum frequences, and noise, of the cosmos. *Quanto Canto* manifests a new apparatus, in the sense of Karen Barad, for apprehending our relationship to the quantum realm. Only by creating new phenomena will we change our orientation towards the universe.

# Quantum computer code

To derive the noise used to modulate the harmonics in the sonic harmonic oscillator, I turned to highly-entangled qubit states known as [GHZ states](https://en.wikipedia.org/wiki/Greenberger%E2%80%93Horne%E2%80%93Zeilinger_state). In my case, I used publicly available 127-qubit IBM quantum computers. I then entangled each of the 127 qubits with each other in two different circuit topologies. In a perfect (quantum) world, if you measured the expectation value of each of the qubits with the first one, it should be equal to 1--that is, everything would be perfectly correlated. But of course this doesn't happen due to noise: the noise of the circuit itself, the noise of the universe entering into the dilution refrigerator, etc. I then took the deviation of the measured expectation values from the expected as my "noise" values.

The [Jupyter notebook](https://gitlab.com/tranxxenolab/quanto_canto/-/blob/master/notebooks/GHZ%20Noise.ipynb?ref_type=heads) for doing this is available on Gitlab, and is heavily based on existing IBM code, but customised for my own use.

In future iterations I would like to be able to access other "noisy" aspects of the quantum computer, focusing specifically on the interface between the transmon qubits and the environment.

# Composition and Tuning

I used just intonation tuning for the *Quanto Canto* composition. The composition consists of three movements that run without a break for around 15 minutes, and then is off for around 5 minutes to allow for some rest for gallery attendants. (Although the ultimate desire is for it to be a continuous, endless composition.) The first movement is tuned using the scaled that La Monte Young used for the *Well-Tuned Piano*, as described by Kyle Gann in "La Monte Young's The Well-Tuned Piano" published in *Perspectives of New Music* in 1993. The second movement tuning is based on an anharmonic "octave" derived from qubit transition frequencies.

Briefly, transmon qubits function by being excited using microwave frequencies. We usually think of the states of these qubits as |0> and |1>. To distinguish between these states we need *anharmonicity*, otherwise no higher energy states could be discerned. But qubits can also go above the |1> state into higher energy states (|2>, |3>, and so forth). IBM used to provide the API for discovering these states on their quantum systems, but unfortunately removed them recently (February 2025). To derive this scale I took the ratio of transmon qubit frequencies between the |1> and |2> states, approximated it using a 61-limit fraction, and then derived a scale from that "octave" relationship using the method described by David Doty in *The Just Intonation Primer: An Introduction to the Theory and Practice of Just Intonation*.

The third section is a 7-limit scale of my own design, basically constructed to explore the possibilities of the 7th harmonic.

All [scales](https://gitlab.com/tranxxenolab/quanto_canto/-/tree/master/scales?ref_type=heads) are available on Gitlab as Scala files. The notebooks for [qubit calibration](https://gitlab.com/tranxxenolab/quanto_canto/-/blob/master/notebooks/calibrating-qubits-pulse_modified.ipynb?ref_type=heads) and [accessing higher energy states](https://gitlab.com/tranxxenolab/quanto_canto/-/blob/master/notebooks/accessing_higher_energy_states_modified.ipynb?ref_type=heads) are available there as well.

# Hardware and design

The sonic aspect of the piece is generated by a custom modular synthesizer module called HARMOGE that is specifically designed for working with harmonic oscillators. HARMOGE allows for customization of which harmonics of the harmonic series to work with, has built-in Scala quantization, and a number of other features. HARMOGE is being developed as part of my modular synthesizer imprint, [selestium modular](https://modular.selestium.net). Since it is under active development the code and hardware design is not currently available, but will be once HARMOGE is publicly available.

Advancement of the saved GHZ noise profiles is triggered by my [XENOKINETICS module](https://modular.selestium.net/modules/xenokinetics/), also under active development.

The noise is output as CV directly to the harmonic inputs of HARMOGE. This comes from a custom fork I made of the [Phazerville firmware for Ornament and Crime](https://github.com/zeitkunst/O_C-Phazerville).

The installation is modular. Each "tube" (all 8 of them, 4 on the bottom, 4 on the top) attaches to a central "hub" using machine bolts. Each hub contains 4 speakers. The "crosses" are connected using two 1m long copper pipes. Magickal sigils are etched into the central length of copper pipes.

[Technical drawings for the physical structure](https://gitlab.com/tranxxenolab/quanto_canto/-/tree/master/CAD?ref_type=heads) of the installation are also available.

# Credits

Concept, design, fabrication, electrical design, programming, composition: Adriana Knouf

Additional design: Felipe Rebolledo

Production assistance: Hedwich Rooks

CNC Fabrication: Made by Mike

Special thanks: Angelique Spaninks, Claudia Pederson, Jaime De Los Rios, Clara Montero, Monica Bello

Commissioned by MU Hybrid Art House, Eindhoven. Supported in part by Stimuleringsfonds Creatieve Industrie and Mondriaan Fonds.

 <img alt="Stimuleringsfonds logo" src="/img/stimuleringsfonds.jpg" width="50%" /> <img alt="Mondriaan logo" src="/img/mondriaanfonds.png" width="20%" />

# Exhibitions

* [Quantum Visions](https://www.tabakalera.eus/en/visiones-cuanticas/), Tabakalera, Donostia/San Sebastián, Spain, 21 February - 8 June, 2025
