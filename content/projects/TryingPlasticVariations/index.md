---
title: "Xenological Entanglements. 001a: Trying Plastic Variations"
description: ""
slug: "trying_plastic_variations"
image: "trying_plastic_variations.jpg"
keywords: ""
tagline: "Lecture-performance on the complexities of molecular jouissance"
categories: 
    - "eromatase"
    - "projects"
    - "biofriction"
date: 2021-02-15T18:02:12+01:00
draft: false
---

{{< load-photoswipe >}}

# *Xenological Entanglements. 001a: Trying Plastic Variations* (2020)

{{< gallery >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___003nzgank.jpg" >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___010nzgank.jpg" >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___016nzgank.jpg" >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___024nzgank.jpg" >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___027nzgank.jpg" >}}
    {{< figure caption="Mesto Žensk performance documentation; credit, Nada Žgank" src="images/201007_Ksenološka_prepletanja___034nzgank.jpg" >}}
{{< /gallery >}}

*this project is part of* [Xenological Entanglements. 001: Eromatase](/projects/eromatase)

*Xenological Entanglements* is a multi-year project exploring the xenomogrification of the tranxxeno body. With altered scientific technologies the artist makes herself the other. In [*001. Eromatase*](https://tranxxenolab.net/projects/eromatase/) at the Kapelica Gallery/Kersnikova Institute, the artist aims to genetically engineer testicular cells (known as Leydig cells) to overproduce oestrogen within a microgravity context. Growing out of the artist’s desire to be one of the first transgender people in space, *Eromatase* begins to construct the necessary tools for a DIY space biology programme, including instrument construction and novel genetic engineering techniques.

Premiering at the [City of Women](http://cityofwomen.org/) festival, *001a: Trying Plastic Variations* is a lecture performance that frames this project within previous enactments of self-experimentation with her voice, hormones, and the audacity of tranxxeno imaginaries of outer space. With vocal transmutations based on the artist’s own self-directed arduous voice training, meditations on her prior work that has orbited the Earth aboard the International Space Station, and early discoveries from the *Eromatase* project, the lecture-performance will enact novel constellations of the body that arise from an understanding of its malleability.

# Performance text

A portion of the lecture-performance involved reading from [this text](https://biofriction.org/biofriction/adriana-knouf-aliens-ontopoietic-self-experimentation-molecular-matrix-voice/) I wrote about the complexities of  molecular jouissance and being intoxicated by estrogen, feeling like an alien, and the potentials for transformation through ontopoiesis.

# Performance Highlights Video

{{< vimeo 466501158 >}}

# Credits

Dramaturgy assistance: Pia Brezavšček, Kira O'Reilly, Isabel Burr Raty

Special thanks: Špela Petrič, Meta Petrič, Simon Gmajner, Živa Brglez, Jure Sajovic, Jurij Krpan

The residency was developed as part of the Biofriction project. Production: Kapelica Gallery/Kersnikova Institute; co-production: City of Women. Special thanks: Department of Art + Design, Northeastern University. Supported by: the European Union – Creative Europe, the Ministry of Culture of the Republic of Slovenia and the Municipality of Ljubljana – Department for Culture.
