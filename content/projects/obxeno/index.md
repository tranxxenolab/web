---
title: "Preparing to Become Tranxxeno: The Obxeno Apparatus"
description: ""
slug: ""
image: "obxeno014_cover.png"
keywords: "lichen, observation, deeper time"
tagline: "entering into lichen time, in-situ"
categories: 
    - "Index"
    - "projects"
date: 2024-04-14T18:33:24+02:00
draft: false
---

{{< load-photoswipe >}}

# Preparing to Become Tranxxeno: The Obxeno Apparatus

{{< gallery >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno000.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno001.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno002.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno003.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno004.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno005.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno006.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno007.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno008.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno009.png" >}}
    {{< figure caption="Obxeno Apparatus object" src="images/obxeno010.png" >}}
    {{< figure caption="Obxeno Apparatus installed in-situ" src="images/obxeno011.png" >}}
    {{< figure caption="Obxeno Apparatus installed in-situ" src="images/obxeno012.png" >}}
    {{< figure caption="Obxeno Apparatus installed in-situ" src="images/obxeno013.png" >}}
    {{< figure caption="Obxeno Apparatus installed in-situ" src="images/obxeno014.png" >}}
    {{< figure caption="Obxeno Apparatus installed in-situ" src="images/obxeno015.png" >}}
{{< /gallery >}}

*Preparing to Become Tranxxeno: The Obxeno Apparatus* explores the long-duration in-situ observation of lichen growth through a biomorphic computational system that additionally provides a habitat for other local species. One desire is to try and enter into "lichen time" by sitting, through the computational device, for months if not years with a single exemplar of lichen. The device takes occasional photos to produce timelapses that additionally show the temporal dynamics of the lichen: its growth, its interaction with other entities, its stillness. This long-duration observation is something that is not often done with lichen, even within a scientific context. The images become part of an archive that can be viewed by people from wherever they live, online. The lichen-device context is designed to meld into the environment and manifests a new way of incorporating computational devices outside of human habitation.

More up-to-date information, images, and timelapses can be found on the Instagram page for the project: <https://www.instagram.com/obxeno>.

Code for *The Obxeno Apparatus* can be found on [gitlab](https://gitlab.com/tranxxenolab/obxenoapparatus).

# Video

{{< gallery >}}
    {{< vimeo 934531886 >}}
{{< /gallery >}}

Here is the first sequence of images from the project.

More up-to-date information, images, and timelapses can be found on the Instagram page for the project: <https://www.instagram.com/obxeno>.


# Acknowledgments

Apparatus design and modeling: Felipe Rebolledo

Location: Zone2Source and Jonmar from Onkruidenier.

The project was supported with an Experiment Grant from Stimuleringsfonds.

![](images/Stimuleringsfonds_logo.jpg)
