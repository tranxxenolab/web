---
title: "Constructed Amateur Warblings: SSTV"
description: ""
slug: "constructed_amateur_warblings_sstv"
image: "ConstructedAmateurWarblingsSSTV.png"
keywords: ""
tagline: "Using SSTV to create hardcore techno"
categories: 
    - "projects"
    - "amateur radio"
    - "sstv"
date: 2021-09-17T12:42:08+02:00
draft: false
---

{{< load-photoswipe >}}

# Constructed Amateur Warblings: SSTV

{{< gallery >}}
    {{< figure caption="Image still from performance video" src="images/constructed_sstv_000.png" >}}
    {{< figure caption="Example of transmitted SSTV image" src="images/constructed_sstv_001.jpg" >}}
    {{< figure caption="Example of transmitted SSTV image" src="images/constructed_sstv_002.jpg" >}}
    {{< figure caption="Example of transmitted SSTV image" src="images/constructed_sstv_003.jpg" >}}
    {{< figure caption="Example of transmitted SSTV image" src="images/constructed_sstv_004.jpg" >}}
{{< /gallery >}}

Digital amateur radio transmissions are primarily designed for efficiency. Even so, their sonic qualities are striking, such as the 1970s synthesizer-like sounds of RTTY, the atonal warblings of JT65 or FT8, or the techno beat of slow-scan television (SSTV). As part of my residency on the Eleonore, I developed variations of SSTV that foregrounded its sonic, aesthetic qualities.

For this live performance as part of [MAKE ME A SIGNAL](https://stwst48x7.stwst.at/make_me_a_signal) on 12.9.2021 I worked with a form of SSTV that has a nice beat of around 140bpm. I wrote computer code that generated particular types of images that resulted in danceable melodies and rhythms. [The code to generate this is available](https://gitlab.com/tranxxenolab/constructed-amateur-warblings).

# Video

{{< vimeo 607322839 >}}

# Credits

Thanks to ShuLea Cheang, Franz Xavier, Tanja Brandmayr, and the entire STWST 48x7 team.
