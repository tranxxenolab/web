---
title: "DEAR INTERLOCUTOR: TX-1"
description: ""
slug: ""
image: "DEAR_INTERLOCUTOR_001.png"
keywords: "video, TX-1"
tagline: "3-channel video about alienness, rural life, and sci-fi futurities"
categories: 
    - "Index"
    - "projects"
date: 2021-09-05T21:48:34+02:00
draft: false
---

{{< load-photoswipe >}}

# DEAR INTERLOCUTOR: TX-1

{{< gallery >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_000.png" >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_001.png" >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_002.png" >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_003.png" >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_004.png" >}}
    {{< figure caption="Still from film; credit, Adriana Knouf" src="images/DEAR_INTERLOCUTOR_005.png" >}}
{{< /gallery >}}

*DEAR INTERLOCUTOR: TX-1* takes the [TX-1](/projects/tx-1) project as the starting point for a series of epistolary meditations on alienness, rural life, what we desire from space travel, and queer futurities. Presented as a three-channel video, the piece includes footage shot at and around Kennedy Space Center in Florida, in the US, excerpts from documentation of my performances, family archival video, and clips from science fiction film and television. The juxtaposition of the channels allows for non-linear temporal relationships between future, past, and present to be drawn out, a key element of the transfeminine experience. A screener version of the piece can be viewed upon request.

# Credits

Editing assistance: Giorgia Piffaretti

Special thanks: Claudia Pederson, Špela Petrič.

Additional funding from Amsterdam Fonds voor de Kunst through a 3Package Deal award, as well as a residency through Art4Med at Waag, Amsterdam.
