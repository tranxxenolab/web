---
title: "Arrival at Kersnikova"
description: "On coming to Ljubljana from the United States during a pandemic to work on xenomogrification"
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "notepad"
date: 2020-09-09T12:00:17+02:00
draft: false
---

{{< load-photoswipe >}}

As I'm writing this, I've actually been at [Kersnikova](https://kersnikova.org/)  for my [Biofriction Residency](https://biofriction.org/) for over a month, so think of this as a delayed entry into my time and my work here.

{{< figure class="float-left mr-2 mb-1 pr-2 pb-1 pt-1 mt-1" width="200px" caption="Adriana, the day of arrival at Kersnikova, and after two weeks of quarantine" src="images/Knouf_Kersnikova.jpg" >}}

Arriving to Ljubljana as a US national during a pandemic--and when I am ostensibly banned from entry to the EU because of that nationality--was quite a challenge. I'm extremely grateful for the work that Kersnikova, and especially Živa Brglez, did to get me the visa that allows me to be here. I had to quarantine for two weeks (as the border guard said, "The US is a bad country right now", and I couldn't disagree), but was graced by terrace visits by friends here.

I'm here to work on ["Xenological Entanglements. 001: Eromatase"](https://tranxxenolab.net/projects/eromatase/), supported by Biofriction and produced by Galerija Kapelica/Zavod Kersnikova. This is likely to be a very long-term project, and I am very happy to be able to get deep into the early stages of the project with Kapelica/Kersnikova. The piece began from my desire, as someone who experiences the world as a transgender person, to be able to travel to space. This comes from deep, old, visceral feelings about my unsuitability for an existence on this planet and thus my intense wish to leave it. Yet I am also on hormone-replacement therapy (HRT), and we don't know if it's even safe for people like me to travel to space, nor do we have any way of producing the hormones I would need during a long-duration space mission. 

And thus enters "xenomogrification", a concept I've been developing in concert with my ideas around *xenology*. With xenomogrification, I desire to transform myself (and other entities, perhaps) into ever stranger, "unnatural", configurations so as to be able to explore the universe-given possibilities for change. This stands as a direct contrast to capitalist systems that allow for some limited range of novelty, but only those changes that still allow profit to be made. Xenomogrification eshews this, and instead enacts the possibilities that a capitalist system would not allow to come into being.

Thus with "Eromatase" we're beginning to set the stage for this xenomogrification. The work consists of two parts: one, building a microgravity simulation system (a clinostat), and two, the exploration of methods to induce testicular cells to produce estrogen rather than testosterone.

{{< figure class="float-left mr-2 mb-1 pr-2 pb-1 pt-1 mt-1" width="200px" caption="First prototype of the clinostat" src="images/beginning_clinostat.jpg" >}}

So far I have been building, with Lovrenc Košenina, the system that forms the basis of our clinostat. We are using an off-the-shelf rotating wall vessel (that we are looking into reverse engineering) with custom hardware control of the rotating. The beginning aspects of this code is available on [gitlab](https://gitlab.com/tranxxenolab/clinostat), and we will eventually post schematics and more details about building your own. We want others to be able to build their own clinostats, given the usual high cost and limited availability of these systems.

{{< gallery >}}
    {{< figure caption="Dead cells alien landscape" src="images/dead_cells000.jpg" >}}
    {{< figure caption="Dead cells alien landscape" src="images/dead_cells001.jpg" >}}
    {{< figure caption="Dead cells alien landscape" src="images/dead_cells002.jpg" >}}
    {{< figure caption="Dead cells alien landscape" src="images/dead_cells003.jpg" >}}
{{< /gallery >}}

Next, in collaboration with Kristijan Tkalec, we have been working with mouse Leydig testicular cells, learning how to resuscitate them, culture them, and propagate them. After a mishap with the wrong temperature of medium, the cells unfortunately died, although we were lucky to have some that we had already put into the -80C freezer. That's allowing us time to plan how to do the next steps of the process: induce estradiol production, measure the results, grow the cells in simulated microgravity. Yet the mishap allowed me to view some truly incredible alien landscapes of the dead Leydig cells spread out within the flask.

Being here for the residency also allows me to experience life and culture, a life that has been so rudely interrupted in the US due to incompetence. Openings, performances, sound walks, museum visits, coffee conversations--these all are possible in a nation (and, in actuality, a continent) that took the necessary precautions early on in the pandemic, and continue to adjust things as needed. Such times enrich my experience and enable transversal connections that have already brought joy, insight, and new possibilities.

There will be much more to come as we develop the project in the coming month and a half.

And I will be presenting a lecture-performance entitled ["Xenological Entanglements. 001a: Trying Plastic Variations"](https://www.facebook.com/events/2642794709384240/) as part of [Mesto Žensk/City of Women](http://cityofwomen.org/) festival. See the [Facebook event page](https://www.facebook.com/events/2642794709384240/) for more info. The performance will be on 07.10.2020 at 2000h CEST and will be live-streamed! More information to come about this in the coming week.

## Credits

The residency was developed as part of the Biofriction project. 

Production: [Kapelica Gallery](http://kapelica.org/index_en.html)/[Kersnikova Institute](https://kersnikova.org/); co-production: [City of Women](http://cityofwomen.org/). Special thanks: Department of Art + Design, Northeastern University. Supported by: the European Union Creative Europe, the Ministry of Culture of the Republic of Slovenia and the Municipality of Ljubljana Department for Culture.
