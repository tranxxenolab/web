---
title: "About the tranxxeno lab"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "about"
date: 2019-12-07T23:15:53-05:00
draft: false
---

The **tranxxeno lab** is a nomadic artistic research laboratory that promotes entanglements amongst entities trans (across, between, beyond) and xeno (strange, alien, other). We are interested in, among many other things: queer and trans futurities; space art; engagements with satellites and drones; the intersection of ancient and modern technologies; encounters with non-humans like plants, slime molds, and artificial intelligences; and care practices for the same. We write, we research, we make, we listen, we build, we sleep, we dance, we fight, we persevere.

All xenoentities are welcome in the tranxxeno lab. We are always looking for new forms of encounter. If you are interested, please contact us.

You can follow us online on Mastodon at [@tranxxenolab@mastodon.social](https://mastodon.social/@tranxxenolab).

<!-- **To signup for occasional e-mails about xenological encounters, please [visit here](https://8c6811b8.sibforms.com/serve/MUIFANoxk3BtQ5PMtqNlfaSssZ2jNAZA3aPVybPOmU0Nu-xJcBHaVfEnRRmcvWkgzGQ0CnK3cg9rtVGCAPjRlf-CDgylNjhC9wu_cosofjdEfmo8qQ3dw1IHTUY2cCwiN3SFaYx7cn-O8MtRrhDZfBqPD2bxee0OcmSIvl86klLlfyibE9Z4qGfhSYr850XAeqmp12a7Ng0KPOhf)**. -->

To signup for occasional e-mails about xenological encounters, please [visit here](/writings/newsletter).
