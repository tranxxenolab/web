---
title: "Saccular Fount"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2021-06-08T15:30:05+02:00
draft: false
---

I'm pleased to present the [premiere of a documentary about my Biofriction residency](https://vimeo.com/559932114) at the Kersnikova Institute/Kapelica Gallery last fall. Included in this documentary is a new work that I created during the residency, and am posting about for the first time, [*Xenological Entanglements. 001b: Saccular Fount*](projects/saccular_fount). This piece is a photoperformance and speculative object about externalized production of hormones under microgravity conditions. It will be [exhibited at Ectopia in Lisbon, Portugal](https://biofriction.org/biofriction/biofriction-exhibition/) that features the other Biofriction artists. Excited to be showing amongst this group!
