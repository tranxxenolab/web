---
title: "Information about DEAR INTERLOCUTOR: TX-1"
description: "Some information posted about the video piece"
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2022-05-30T22:29:56+02:00
draft: false
---

I wanted to share some information about a three-channel video piece that I completed last year entitled [*DEAR INTERLOCUTOR:TX-1*](/projects/dear_interlocutor/), a piece the builds off of the [*TX-1*](/projects/tx-1) project to consider aspects of ruralness, alienness, querness, and sci-fi futurities. I also posted the [script](/writings/dear_interlocutor/) for the piece. If you're interested in watching a screener please reach out to me.

I'll be sharing some more information soon about two new projects that I'm developing!
