---
title: "What We Do in the Shadows news"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2020-03-27T16:47:06-05:00
draft: false
---

I have just posted the first of an occasional series of Quaronatine Diaries, which will be short texts about our present moment from a tranxxeno perspective. Called ["What We Do in the Shadows"](/writings/shadows/), the text considers the heteronormative familial aspects of our present forced isolation and what this means for tranxxeno peoples.
