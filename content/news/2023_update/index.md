---
title: "Index"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2023-08-05T22:59:00+02:00
draft: false
---

There is a lot to update since my last news posting, but for the moment I simply want to note that in 2022 I finished production on ["TX-2: MOONSHADOW](/projects/tx-2/) and the accompanying film, ["Temporal Transfer Orbit (TTO)"](/projects/tto/). In the meantime, ["TX-1"](/projects/tx-1/) was selected for the [2023 Lumen Prize BCS Futures Award Longlist](https://www.lumenprize.com/2023bcs-futures-award-longlist). I've had quite a lot of talks, exhibitions, and presentations over the past year, and at some point I should update the site with all of that information. I've also been developing my own modular synthesizer modules which will soon be available on [modular.selestium.net](https://modular.selestium.net), as well as doing a lot of research for future projects. More to come soon!
