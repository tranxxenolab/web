---
title: "Fragments of Xenology Released"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2021-04-28T14:56:18+02:00
draft: false
---

I've decided to self-publish my text ["Fragments of Xenology"](writings/fragmentsofxenology/), a series of short theoretical musings on this concept of "xenology". While I began it in 2019 and finished it just before the start of the pandemic in 2020, and it has not been updated since then, I still feel like it retains a lot of relevance, especially for the work of the tranxxeno lab. But I hope that by publishing it here others might find concept-provoking thoughts in it as well. If you do have something you want to share after reading it, please feel free to reach out to me!
