---
title: "Index"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2024-09-09T21:07:13+02:00
draft: false
---

Yet again I have failed to update the news section of this website in a timely manner. But just a few things of note: I've installed [Preparing to Become Tranxxeno: The Obxeno Apparatus](https://tranxxenolab.net/projects/obxeno/) in the Shadow Garden in Amstelpark in Amsterdam, an off-grid, long-duration project to sit and watch lichen. More information can be found on the project page as well as on the [Instragram page](https://www.instagram.com/obxeno) for the project. As well, earlier this year I presented as part of the Sonic Acts Biennial, and the documentation of my [lecture-performance](https://www.youtube.com/watch?v=eCSqGhNUuuQ) and my [interview](https://www.youtube.com/watch?v=D3sBUbxQ4Ss) are now available online. I have group shows coming up this fall in London and Amsterdam, as well as a solo show in Vienna. Stay tuned to my socials for more details!
