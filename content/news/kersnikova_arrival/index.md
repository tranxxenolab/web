---
title: "Arrival at Kersnikova"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2020-09-22T12:46:16+02:00
draft: false
---

I arrived in early August to Ljubljana for my Biofriction residency--read a bit about what I've been doing since I got here in [this note](notepad/biofriction-start) in the Notepad.
