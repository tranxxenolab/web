---
title: "TX-1 Launch"
description: ""
slug: "tx-1_launch"
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2020-03-06T15:06:34+02:00
draft: false
---

I am beyond delighted to announce that our project *TX-1* is launching to the International Space Station! For more information see the [project page](/projects/tx-1).
