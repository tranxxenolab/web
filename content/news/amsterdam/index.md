---
title: "Arrival to Amsterdam"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2021-01-29T09:21:06+01:00
draft: false
---

A short update to say that I am now living in Amsterdam! After an incredible fall at the Kersnikova Institute/Kapelica Gallery, and a lot of development on the [Eromatase](https://tranxxenolab.net/projects/eromatase/) project, I have now settled in Amsterdam as a [3Package Deal artist](https://www.amsterdamsfondsvoordekunst.nl/fondsinitiatieven/3package-deal/3package-deal-2020-vijftien-nieuwe-talenten-van-start/) who will be in residence at [Waag](https://waag.org/en/adriana-knouf). I will be working as part of an EC-funded project called Art4Med. Soon I will post more about my work from the fall and what I am planning on exploring while at Waag. Also: I am now registered in the Netherlands as a sole-proprietor business, KvK number 81623542.
