---
title: "Writing in 'Prospections'"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "Index"
    - "news"
date: 2021-12-07T12:04:06+01:00
draft: false
---

I'm quite honored to have been asked to contribute to the "No Linear Fucking Time" issue of Prospections by [BAK](https://www.bakonline.org/) which is part of the exhibition and program of the same name. My contribution is about lichen, deeper times, and the epistolary. It's entitled ["Dearest Xen (Letters to Lichen)"](https://www.bakonline.org/prospections/dearest-xen-letters-to-lichen/), and is one of the more experimental things I've published. Be sure to check out the [other contributions to this issue as well](https://www.bakonline.org/focus/no-linear-fucking-time/), it's quite a distinguished group.
