---
title: "Adriana Knouf"
description: "Founding Facilitator"
slug: ""
image: "headshot.jpg"
keywords: ""
categories: 
    - "people"
date: 2019-12-03T23:38:22-05:00
website: "http://tranxxenolab.net"
email: "asknouf@tranxxenolab.net"
social: 
    twitter: "zeitkunst"
    instagram: "zeitkunst"
sortName: "Knouf"
draft: false
---

Mastodon: [@zeitkunst@post.lurk.org](https://post.lurk.org/@zeitkunst)

Bluesky: [@zeitkunst.bsky.social](https://bsky.app/profile/zeitkunst.bsky.social)

Instagram: [@zeitkunst](https://www.instagram.com/zeitkunst/) (phasing out in 2025)

Adriana Knouf, PhD (NL/US) works as an artist, writer, musician, and xenologist. She attunes herself to electromagic frequencies; studies the interferences of temporalities future, past, and present; and experiments with entities bio, silico, litho, cosmic. She is the Founding Facilitator of the tranxxenolab, a nomadic artistic research laboratory that promotes entanglements among entities trans and xeno. Adriana regularly presents her artistic research around the world and beyond, including a work that has flown aboard the International Space Station. Her work has been recognized by a number of awards, including an Award of Distinction at Prix Ars Electronica (2021), an Honorary Mention at Prix Ars Electronica (2005), BCS Futures Award Longlist selection as part of the Lumen Prize (2023), an Honorary Mention from the Science Fiction Research Association's Innovative Research Award (2021), and as a prize winner in The Lake’s Works for Radio #4 (2020). She additionally performs with her modular synthesizer as [Selestra](https://selestra.bandcamp.com/), and designs and sells modular synthesizer modules through her company [selestium modular](https://modular.selestium.net).


<!--
Adriana Knouf, PhD (NL/US) works as an artist, writer, and xenologist. She engages with topics such as wet media, space art, satellites, radio transmission, non-human encounters, drone flight, queer and trans futurities, machine learning, the voice, and papermaking. She is the Founding Facilitator of the tranxxenolab, a nomadic artistic research laboratory that promotes entanglements among entities trans and xeno. All xenoentities are welcome in the tranxxeno lab.

Adriana regularly presents her artistic research around the world and beyond, including a work that has flown aboard the International Space Station. Her work has been recognized by a number of awards, including an Award of Distinction at Prix Ars Electronica (2021), an Honorary Mention at Prix Ars Electronica (2005), BCS Futures Award Longlist selection as part of the Lumen Prize (2023), an Honorary Mention from the Science Fiction Research Association's Innovative Research Award, and as a prize winner in The Lake’s Works for Radio #4 (2020).
-->

Adriana is currently based in Amsterdam, the Netherlands.

You can find her CV [here](ask-CV.pdf). She can be followed on Mastodon as [@zeitkunst@post.lurk.org](https://post.lurk.org/@zeitkunst).

Photo credit: Christian Brems.
