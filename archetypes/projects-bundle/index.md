---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
image: ""
keywords: ""
tagline: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "projects"
date: {{ .Date }}
draft: false
---
