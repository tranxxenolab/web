---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "news"
date: {{ .Date }}
draft: false
---
