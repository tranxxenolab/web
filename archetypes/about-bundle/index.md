---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "about"
date: {{ .Date }}
draft: false
---
