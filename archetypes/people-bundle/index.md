---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "people"
date: {{ .Date }}
draft: false
website: ""
email: ""
social:
    twitter: ""
    instagram: ""
sortName: ""
---
