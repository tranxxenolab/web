---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
slug: ""
image: ""
keywords: ""
categories: 
    - "{{ replace .TranslationBaseName "-" " " | title }}"
    - "writings"
date: {{ .Date }}
publicationYear: ""
inProgress: true
draft: false
---
